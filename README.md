# generator-plugit-module [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Plugit Module Yeoman Generator.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-plugit-module using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-plugit-module
```

Then generate your new project:

```bash
yo plugit-module
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

unlicense © [Marley Plant](https://marleyplant.com)


[npm-image]: https://badge.fury.io/js/generator-plugit-module.svg
[npm-url]: https://npmjs.org/package/generator-plugit-module
[travis-image]: https://travis-ci.com/MarleyPlant/generator-plugit-module.svg?branch=master
[travis-url]: https://travis-ci.com/MarleyPlant/generator-plugit-module
[daviddm-image]: https://david-dm.org/MarleyPlant/generator-plugit-module.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/MarleyPlant/generator-plugit-module
[coveralls-image]: https://coveralls.io/repos/MarleyPlant/generator-plugit-module/badge.svg
[coveralls-url]: https://coveralls.io/r/MarleyPlant/generator-plugit-module
